# Translation of StatusNet - SQLProfile to Spanish (Español)
# Exported from translatewiki.net
#
# Author: Armando-Martin
# --
# This file is distributed under the same license as the StatusNet package.
#
msgid ""
msgstr ""
"Project-Id-Version: StatusNet - SQLProfile\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2012-03-11 20:13+0000\n"
"PO-Revision-Date: 2012-03-11 20:16:19+0000\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POT-Import-Date: 2011-06-05 21:51:39+0000\n"
"X-Translation-Project: translatewiki.net <https://translatewiki.net>\n"
"X-Generator: MediaWiki 1.20alpha (r113583); Translate 2012-03-02\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. TRANS: Plugin description.
msgid "Debug tool to watch for poorly indexed DB queries."
msgstr ""
"Herramienta de depuración de errores para peticiones a bases de datos mal "
"indexadas."
